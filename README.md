# Scripts to help manage Git's ident attribute

Git has an `ident` attribute by which it replaces `$Id$` in the blob object
with `$Id: xxx` where `xxx` is the 40-character hexadecimal SHA1 blob object
name.

This is useful for me since I have co-authors that don't use `git`. I can now
email them a file with the blob object SHA1 name at the bottom. When I get
their modifications back by email, I find the commit that references this blob
object using `git-find-commit.pl`, and create a branch based there for their
modifications. Explicitly:

    $ git find-commit -v <co-authors-file.tex>
    Last commit that uses 9b8647839aad689a52d0dd62f544e4e278c84ee0:
    711dad28daec27f638ac7c5997db00e3d808d88f
    $ git checkout -b author-b1 711dad28
    # save their changes in the local repository
    $ git commit --author <authorname> -a
    $ git checkout master
    $ git merge author-b1
    # Email the file back to them

This avoids confusion in case an author edits an older version, etc. (Ideally
all co-authors should use git, but I don't think that will ever happen.)

**NOTE:** After a `git commit` git won't automatically update the `$Id$`
attribute. You need to install the `post-commit` hook described below.

## Usage

1. Edit `.gitattributes`, `.git/info/attributes` or `~/.config/git/attributes`
   and add `ident` for all file types you want substituted:

        *.tex ident
        *.bib ident

2. Put a line containing `$Id$` in your file. It will be replaced by the
   commit ID the current head whenever the file is checked out.

## Updating `$Id$` after commits.

The `$Id$` is only updated on check outs **NOT CHECKINS**. Thus if you make
changes and `git commit`, the `$Id$` token in files in your local repository
will be unchanged. If want it updated, you need to re-checkout the file:

    git checkout HEAD -- file1 file2 ...

This is **DANGEROUS**, since if these files have local modifications they will
be silently overwritten! For this reason, I wrote the `git-refilter.pl`
script. By default, this re-checks out all files modified in HEAD that have a
check-in attribute set (see the man page of `gitattributes`). A convenient way
to run it is to set up a git-alias:

    git config --set alias.refilter '! /path/to/git-ident/git-refilter.pl'

or equivalently put a symlink to `git-refilter.pl` (called `git-refilter`)
somewhere in your path.

### Keeping `$Id$` automatically updated.

If you want `$Id$` to be *automatically* updated after commits, then you can
run `git-refilter.pl` from your `post-commit` hook. If you don't have this
hook, simply create a symlink in `.git/hooks/post-commit` it to
`git-refilter.pl`

### Updating `$Id$` in `git show`

If you `git show @~1:file` to temporarily examine it, then the `$Id$` tag
won't have the blob object name anymore. If you want to store the file for
longer and need the `$Id$` tag updated for reference, you can use the
`git-extract.pl` script. This runs `git archive` and then extracts the archive
to `stdout`.

If you want to extract multiple files, do something like

    git archive --prefix=old/ @~10 file1 ... | tar -xv

## Finding the commit of a blob object

The `git-find-commit.pl` script takes a filename (or a blob object name) as an
argument. If given a filename, it looks for an `$Id: xxx $` tag in the file
and gets the blob object from it. Then it searches the commit history and
prints the most recent commit that uses this blob (or all commits, if the `-a`
option is specified).

A convenient way to run it is to set up a git-alias, or put a symlink to it in
your path.
