#! /usr/bin/perl -w
# Created   : Thu 26 May 2016 09:04:42 PM EDT
# Modified  : Wed 09 Oct 2019 01:32:38 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'
# Find the most recent commit using blob. Argument can be a file or a SHA1 Id

use Getopt::Std;

# Process options: -a: all, -v: verbose
my %opt = ();
getopts( 'avhd', \%opt ) or exit(1);
exec( qw(perldoc -T -o term), $0 )
    if( $opt{h} );

my ($sha1, $matched);

if ( $ARGV[0] and $ARGV[0] =~ m/^[0-9a-f]{40}$/i )
{
    $sha1 = $ARGV[0];
}
else
{
    # Arg is a filename or not present
    chdir( $ENV{GIT_PREFIX} ) if( exists( $ENV{GIT_PREFIX} ));
    while( my $line = <> )
    {
	last if( ($sha1) = ($line =~ m/\$Id:\s*([0-9a-f]{40})\s*\$/i) );
    }
    die( "Couldn't find SHA1\n" ) if( ! $sha1 );
}

if( $opt{d} )
{
    # Use git describe
    exec(qw(git describe --always), $opt{a} ? ('--all', $sha1) : $sha1 );
}
else
{
    # Search manually
    print( $opt{a} ? 'All commits that use ' : 'Last commit that uses ',
        $sha1, ":\n" ) if( $opt{v} );

    foreach my $commit (qx(git rev-list --all))
    {
        chomp( $commit );
        if( qx(git rev-list -1 --objects $commit) =~ m/^$sha1/m )
        {
    	print( "$commit\n" );
    	$matched = 1;
    	last unless( $opt{a} );
        }
    }

    die( "No commit uses blob $sha1\n" ) if( !$matched );
}

# {{{1 POD documentation
=head1 NAME

git-find-commit.pl - Find last commit using blob object


=head1 DESCRIPTION

B<git-find-commit.pl> takes a filename (or a blob object name) as an
argument. If given a filename, it looks for an C<$Id: xxx $> tag in the file
and gets the blob object from it. Then it searches the commit history and
prints the most recent commit that uses this blob (or all commits, if the I<-a>
option is specified).

=head1 OPTIONS

=over

=item I<-a>

Find all commits, not just the last one

=item I<-d>

Use C<git-describe --always> to find commit.

=item I<-v>

Be more verbose

=back
