#! /usr/bin/perl -w
# Created   : Fri 10 Jun 2016 10:18:55 PM IST
# Modified  : Sat 11 Jun 2016 11:48:18 AM IST
# Author    : GI <a@b.c>, where a='gi1242+perl', b='gmail', c='com'

die( << 'EOF'
Usage: git extract commit file > output
For multiple files use
    git archive --prefix='old/' <commit> | tar -xv
EOF
) if( scalar( @ARGV ) != 2 || $ARGV[0] eq '-h' );

my $cmd = 'git archive ' . join( ' ', @ARGV[0..1] ) . '| tar -xO';
warn( $cmd . "\n" );
exec( $cmd );
